<?php

/**
 * Config for client rest api
 * @author Przemysław Sobolewski <kontakt@przemyslawsobolewski.com>
 */
class configApi
{
    private static $instance;
    
    private $config = [
        "serviceLogin"    => "rest",
        "servicePassword" => "vKTUeyrt",
        "serviceUrl"      => "http://grzegorz.demos.i-sklep.pl/rest_api/shop_api/v1"
    ];
 
    static function getInstance() 
    {
        if( null === self::$instance )
        {
            self::$instance = new configApi();
            
        }
        
        return self::$instance;
        
    }
    
    private function __construct() {}
    private function __clone() {}
    
    /**
     * Get config api
     * @param  string $configName
     * @return string|array
     */
    function getConfig($configName)
    {
        return $configName ? $this->config[$configName] : $this->config;
        
    }
    
    /**
     * Set config api data
     * @param array $configData [name, value]
     */
    function setConfig($configData)
    {
        if( true === $this->configExist($configData) )
        {
            $this->config[$configData['name']] = $configData['value'];
            
        }
        
    }
    
    /**
     * Check if a config option exists
     * @param type $configData
     * @retrun boolean
     */
    private function configExist($configData)
    {
        if( isset($configData['name']) && isset($configData['value']) && isset($this->config[$configData['name']]) )
        {
            return true;
            
        }
        
        return false;
        
    }
    
}