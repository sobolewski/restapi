<?php

// load exception class
require_once('exception.php');

/**
 * CURL connect
 * @author Przemysław Sobolewski <kontakt@przemyslawsobolewski.com>
 */
class curl
{
    const VERSION = 1.0;
    /**
     * User Agent for curl
     */
    const USER_AGENT = 'Mozilla/4.0 (compatible; MSIE 5.01; Windows NT 5.0)';
    
    private $handle;
    private $responseObject;
    private $responseInfo;
    
    /**
     * @var configApi object 
     */
    private $config;
    
    /**
     * @var array CURL options 
     */
    protected $httpOptions = [
        CURLOPT_HTTPAUTH       => CURLAUTH_BASIC,
        CURLOPT_USERAGENT      => self::USER_AGENT,
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_FOLLOWLOCATION => false
    ];
    
    function __construct() 
    {
        // set default config
        $this->config = configApi::getInstance();
                
    }
    
    /**
     * Set connection options to httpOptions
     * @return void
     */
    private function setConnectionOptions()
    {
        // set login:password for url to connection curl
        $this->httpOptions[CURLOPT_USERPWD] = $this->config->getConfig('serviceLogin') . ":" . $this->config->getConfig('servicePassword');
        $this->httpOptions[CURLOPT_URL]     = $this->config->getConfig('serviceUrl');
        
    }
    
    /**
     * Execute curl
     * @param  string $method
     * @return response object
     * @throws RestClientException
     */
    function curlExecute($method)
    {
        // set connection options
        $this->setConnectionOptions(); 
        
        //open connection
        $this->handle = curl_init();
        
        // choose cURL request method
        if( true === method_exists($this, $method) )
        {
            $this->$method();
            
        } else {
            throw new RestClientException("Error setting cURL request method.");
            
        }
        
        // setopt 
        curl_setopt_array($this->handle, $this->httpOptions);

        // execute
        $this->responseObject = curl_exec($this->handle);
        
        // parse message
        $this->httpParseMessage();
        
        // close connection
        curl_close($this->handle);
        
        // return response object
        return $this->responseObject;
        
    }
    
    /**
     * Method POST
     * @param  $fields
     * @return void
     */
    private function post()
    {
        $this->httpOptions[CURLOPT_POST] = true;
        
        if( null !== json_decode($this->parametersFields) )
        {
            $this->httpOptions[CURLOPT_HTTPHEADER] = array('Content-type' => 'application/json');
            
        }
        
        $this->httpOptions[CURLOPT_POSTFIELDS] = $this->parametersFields;
        
    }
    
    /**
     * Method GET
     * @return void
     */
    private function get()
    {
    }
    
    /**
     * Parse cURL message
     * @throws HttpServerException
     * @throws HttpServerException404
     */
    private function httpParseMessage()
    {
        if(!$this->responseObject)
        {
            throw new HttpServerException(curl_error($this->handle), -1);

        }

        $this->responseInfo = curl_getinfo($this->handle);
        
        $code = $this->responseInfo['http_code'];
        
        if($code == 404)
        {
            throw new HttpServerException404(curl_error($this->handle));

        }

        if(!in_array($code, range(200, 207)) || ($code >= 400 && $code <= 600))
        {
            throw new HttpServerException('Server response status was: ' . $code . ' with response: [' . $this->responseObject . ']', $code);

        }

    }
    
}