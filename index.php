<?php

// switch display error on|off
@ini_set('display_errors', 'on');

// load rest Api class
require_once 'restApi.php';

// init client rest api
$restApi = new restApi();

// POST
$postFields = [   
    'name'          => 'TestApiRest-PS',
    'site_url'      => 'http://google.com/',
    'logo_filename' => 'testPicture.jpg',
    'source_id'     => null
];
//echo $restApi->producers('post', $postFields); die;

// GET
echo $restApi->producers('get');