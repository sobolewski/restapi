<?php

// load require class
require_once 'loadRequireClass.php';

/**
 * Client rest api
 * @author Przemysław Sobolewski <kontakt@przemyslawsobolewski.com>
 */
final class restApi extends curl
{
    // producer value objects
    protected $parametersFields;
    
    /**
     * Producers 
     * @param  string $method
     * @return object
     */
    function producers($method, $parameters = [])
    {
        // set producers config
        $config = configApi::getInstance();
        $config->setConfig(['name'  => 'serviceUrl', 
                            'value' => 'http://grzegorz.demos.i-sklep.pl/rest_api/shop_api/v1/producers']);
        
        // check param exist
        if(is_array($parameters))
        {
            $this->parametersFields = json_encode(['producer' => $parameters]);
            
        }
        
        return $this->curlExecute($method);
        
    }
    
}