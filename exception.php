<?php

class HttpServerException extends Exception 
{
}

class RestClientException extends Exception 
{
}

class HttpServerException404 extends Exception
{
    public function __construct($message = 'Not Found')
    {
        parent::__construct($message, 404);

    }

}